import React, { Component } from "react";
import TodoItems from "./TodoItems"
import './TodoList.css';
class TodoList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      value: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.addItem = this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value})
  }

  addItem(e) {
    if (this._inputElement.value !== "") {
      var newItem = {
        text: this._inputElement.value,
        key: Date.now()
      };

      this.setState((prevState) => {
        return {
          items: prevState.items.concat(newItem)
        };
      });
    }

    this._inputElement.value = "";
    this.state.value = "";
    e.preventDefault();
  }

  deleteItem(key) {
    console.log("Key deleteItem " + key)
    var filteredItems = this.state.items.filter(function (item) {
      return (item.key !== key)
    });

    this.setState({
      items: filteredItems
    });
  }

  render() {
    return (
      <div className="todoListMain">
        <div className="header">
          <form onSubmit={this.addItem}>
            <input type="text" ref={(a) => this._inputElement = a} value={this.state.value} onChange={this.handleChange}
              placeholder="Wpisz zadanie">
            </input>
            <button type="submit">add</button>
          </form>
        </div>
        <TodoItems entries={this.state.items}
                  delete={this.deleteItem} />
      </div>
    );
  }
}

export default TodoList;